#ifndef SIGNALINTR_H
#define SIGNALINTR_H
#include <signal.h>

typedef	void	Sigfunc(int);	/* for signal handlers */

Sigfunc *signal_intr(int signo, Sigfunc *func);

#endif // SIGNALINTR_H
