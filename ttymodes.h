#ifndef TTYMODES_H
#define TTYMODES_H
#include <termios.h>

int tty_cbreak(int fd);     /* put terminal into a cbreak mode */
int tty_raw(int fd);		/* put terminal into a raw mode */
int tty_reset(int fd);		/* restore terminal's mode */
void tty_atexit(void);		/* can be set up by atexit(tty_atexit) */
#ifdef	ECHO	/* only if <termios.h> has been included */
struct termios *tty_termios(void);
#endif

#endif // TTYMODES_H
