#ifndef WRITEN_H
#define WRITEN_H
#include <sys/types.h>

ssize_t writen(int fd, const void *ptr, size_t n); /* Write "n" bytes to a descriptor  */

#endif // WRITEN_H
